package com.mecreativestudio.qr_code_generator_module;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import com.google.zxing.WriterException;
import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import androidmads.library.qrgenearator.QRGSaver;

public class QrCodeGenerator extends AppCompatActivity {

    Bitmap bitmap;
    String value;
    int dimension;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_code_generator);

        ImageView qrCodeImageView = findViewById(R.id.qrCodeImageView);

        Intent intent = getIntent();

        //Check if the intent contains a value for the Qr Code
        if(intent.hasExtra("value")){
            value = intent.getStringExtra("value");

            //Check if the value contains a dimension for the Qr Code
            if(intent.hasExtra("dimension")){
                dimension = intent.getIntExtra("dimension", 500);

                //If the required parameters are here, create and display the Qr Code
                QRGEncoder qrgEncoder = new QRGEncoder(value, null, QRGContents.Type.TEXT, dimension);
                try {
                    //Generating the QrCode in bitmap format
                    bitmap = qrgEncoder.encodeAsBitmap();
                    //Setting Bitmap to the imageView
                    qrCodeImageView.setImageBitmap(bitmap);
                } catch (WriterException e) {
                    Log.v("QR Code Generator", e.toString());
                }

                //Save the Qr Code in the user's phone
                //Define SAVE_PATH and type of Image(JPG or PNG).
                //QRGSaver.save(SAVE_PATH, value, bitmap, QRGContents.ImageType.IMAGE_JPEG);

            }else{
                Log.e("QR Code Generator", "You must provide a Qr Code dimension (in px)");
            }
        }else{
            Log.e("QR Code Generator", "You must provide a value to encrypt in the Qr Code");
        }
    }
}
