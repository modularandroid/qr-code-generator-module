# Qr Code Generator Module

A configurable and customizable Qr Code Generator module for android.



## Requirements

You must add the maven repository to your project. To do so, from Android Studio in your **root gradle file, **add the following line: 

```java
allprojects {
     repositories {
		...
        maven { url "https://jitpack.io" }
	}
}
```



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module
    
  2. Choose **qr-code-generator-module** directory and click Finish.
     *In case of any import error, just proceed to step 3 and it should be automatically solved.*
    
  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

```java
include ":app", ":qr-code-generator-module"
```
  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

```java
dependencies {
    implementation project(":qr-code-generator-module")
}
```

  5. Click Sync Project with Gradle Files (Sync Now)  
    
  6. In your **app AndroidManifest.xml**,  under application, add this line : 

```java
<application
    ...>
    <activity android:name="com.mecreativestudio.qr_code_generator_module.QrCodeGenerator"/>
</application>
```



## Usage

In the activity in which you want to call the module, import it by adding the following line :

```java
import com.mecreativestudio.qr_code_generator_module.QrCodeGenerator;
```



 Then you can call this module with a simple intent :


```java
Intent intent = new Intent(YOUR_ACTIVITY_NAME.this, QrCodeGenerator.class);

//The value to encrypt [String]
intent.putExtra("value", YOUR_VALUE);

//Dimension in pixels [Default value: 400]
intent.putExtra("dimension", QR_CODE_DIMENSION_VALUE);

startActivity(intent);
```

*Note that you must change YOUR_ACTIVITY_NAME by the name of the activity that calls the module. Of course, you must also change YOUR_VALUE and QR_CODE_DIMENSION.*



### 	Example

Let's say that your application contains an activity, called ***MainActivity***, in which a ***qrCodeGeneratorButton*** button is implemented. You want to generate and display the Qr Code associated with your value when you click on this button. 

By following the previous instructions, the code which you should have to do so is the following : 

```java
import ...
import com.mecreativestudio.qr_code_generator_module.QrCodeGenerator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         //Add a click listener on the qrCodeGeneratorButton
        Button qrCodeGeneratorButton = findViewById(R.id.qrCodeGeneratorButton);
        qrCodeGeneratorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //When a click is made on this button, open the qr-code-generator-module
                Intent intent = new Intent(MainActivity.this, QrCodeGenerator.class);
                intent.putExtra("value", "Hello world !");
                intent.putExtra("dimension", 600);
                startActivity(intent);
            }
        });
    }
}
```



## Configuration and Customization

This Qr Code Generator Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the QrCodeGenerator Java class which is located here :

```java
YOUR_PROJECT/qr-code-generator-module/java.com.mecreativestudio.qr_code_generator_module/QrCodeGenerator.java
```

*A Qr Code save function is already implemented and commented there*



If you want to adapt its layout to your needs, you can simply edit the qr_code_generator.xml file which is located here :

```java
YOUR_PROJECT/qr-code-generator-module/res/layout/qr_code_generator.xml
```